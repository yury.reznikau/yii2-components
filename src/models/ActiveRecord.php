<?php

namespace common\components\models;
use yii\behaviors\TimestampBehavior;
use Yii;

class ActiveRecord extends \yii\db\ActiveRecord {
    
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],    
            ]
        ];
    }
    
    public function attributeLabels() {
        return [
            'createdBy' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'createdString' => Yii::t('app', 'Created At'),
            'updatedString' => Yii::t('app', 'Updated At'),
        ];
    }

    
    public function getCreatedString() {
        return $this->formatDate($this->created_at);
    }
    
    public function getUpdatedString() {
        return $this->formatDate($this->updated_at);
    }
    
    protected function formatDate($timestamp) {
        return Yii::$app->formatter->asDatetime($timestamp);
    }
    
    public function getIsAdmin($default = true) {
        return YII_IS_WEBAPP  ? 
                   (Yii::$app->user->identity ? Yii::$app->user->identity->isAdmin : false) 
                   : $default;
    }
    
    public static function findOrCreate(array $condition, array $attributes = NULL) {
        
        if (!$model = static::findOne($condition)) {
            $model = new static;
            if (is_null($attributes)) {
                $attributes = $condition;
            }
            $model->setAttributes($attributes);
            
            if (!$model->save()) {
                throw new \Exception("Failed to create ".static::className());
            }
        }
        
        return $model;
        
    }
    
}
