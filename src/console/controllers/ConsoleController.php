<?php

namespace common\components\console\controllers;
use Yii;

class ConsoleController extends \yii\console\Controller {

    protected $logCategory;
    
    public function init() {
        parent::init();
        if (is_null($this->logCategory)) {
            $this->logCategory = $this->id;
        }
    }
    
    public function runAction($id, $params = array()) {
        
        $result = NULL;
        
        try {
            
            $result = parent::runAction($id, $params);
            
        } catch (\Exception $e) {
            
            $message = $e->getMessage();
            $trace = $e->getTraceAsString();
            
            Yii::error($message, $this->logCategory);
            Yii::trace($trace, $this->logCategory);
            
            if (YII_DEBUG) {
                $this->stdout($message, \yii\helpers\Console::BG_RED);
                $this->stdout($trace);
            } else {
                throw $e;
            }
            
        }
        
        return $result;
    }
    
    public function beforeAction($action) {
        $this->logCategory = "$this->id.$action->id";
        return parent::beforeAction($action);
    }
    
    public function stdout($string) {
        Yii::info($string, $this->logCategory);
        parent::stdout($string.PHP_EOL);
    }
    
}
