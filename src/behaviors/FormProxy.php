<?php

namespace common\components\behaviors;
use yii\base\Behavior;

class FormProxy extends Behavior {
    
    public $modelClass;
    protected $proto;
    
    public function attach($owner) {
        parent::attach($owner);
        $this->proto = new $this->modelClass;
    }
    public function getDb() {
        return $this->proto->db;
    }
}
