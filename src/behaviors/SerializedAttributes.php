<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\behaviors;

/**
 * Description of SerializedAttributes
 *
 * @author Vladislav
 */
class SerializedAttributes extends TransformedAttributes {
    
    public $filter = false;
    
    public function init() {
        
        $filter = $this->filter;
        
        $this->processCallback = function($value) use ($filter) {

            if ($filter && is_array($value)) {
                $value = array_filter($value);
            }

            $result = is_array($value) ? implode(",", $value) : $value;
            return $result;
        };
        
        $this->unprocessCallback = function($value) use ($filter) {
            
            $result = is_scalar($value) ? explode(",", $value) : $value;
            
            if ($filter) {
                $result = array_filter($result);
            }

            return $result;
        };

        parent::init();
    }
    
}
 
