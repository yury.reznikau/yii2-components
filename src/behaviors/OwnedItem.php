<?php

namespace common\components\behaviors;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use Yii;

class OwnedItem extends Behavior {
    
    public $userIdField = 'user_id';
    public $userClass;
    
    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'setOwnerId',
            ActiveRecord::EVENT_BEFORE_INSERT => 'setOwnerIdForce',
        ];
    }
    
    public function setOwnerIdForce(\yii\base\ModelEvent $event) {
        if (!YII_IS_WEBAPP && $this->owner->user_id) {
            //allow model manipulations for console migrations etc
            return;
        }
        
        $this->setOwnerId($event, true);
    }
    
    public function setOwnerId(\yii\base\ModelEvent $event, $force = false) {
        
        if ($force || !$this->owner->user_id) {
            $this->owner->user_id = Yii::$app->user->id;
        }
    }
    
    public function getOwner() {
        return $this->owner->getUser();
    }
    
    public function getCreatedBy() {
        return $this->owner->user->username;
    }
    
}
