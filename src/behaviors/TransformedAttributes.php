<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\behaviors;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

class TransformedAttributes extends Behavior {
    
    public $attributes = [];
    public $processCallback;
    public $unprocessCallback;
    public $skipNull = true;
    
    
    public function init() {
        parent::init();
        
        if (!is_callable($this->processCallback)) {
            throw new ServerErrorHttpException("Callable process callback property must be set for ".  get_class($this). ' to function properly');
        }
        
        if (!is_callable($this->unprocessCallback)) {
            throw new ServerErrorHttpException("Callable unprocess callback property must be set for ".  get_class($this). ' to function properly');
        }
    }
    
    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_FIND      => 'unprocessAttributes',
            ActiveRecord::EVENT_BEFORE_INSERT   => 'processAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE   => 'processAttributes',
        ];
    }
    
    public function processAttributes() {
        $this->process($this->processCallback);
        return true;
    }
    
    public function unprocessAttributes() {
        $this->process($this->unprocessCallback);
        return true;
    }
    
    protected function process($callback) {
        foreach ($this->attributes as $attribute) {
            $value = $this->owner->getAttribute($attribute);
            if (!$this->skipNull || !is_null($value)) {
                $this->owner->setAttribute($attribute, call_user_func($callback, $value, $this->owner));
            }
        }
        return true;
    }
    
}
