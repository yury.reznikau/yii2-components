<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace common\components\Validators;
use yii\validators\Validator;
use Yii;

/**
 * Description of ObjectValidator
 *
 * @author Vladislav
 */
class ObjectValidator extends Validator {
    
    public function validateValue($value) {
        
        $result = NULL;
        
        if (!is_object($value)) {
            
            $result = [Yii::t('app', "Provided value is not a valid object"), []];
        }
        
        return $result;
    }
    
}
