<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\actions;
use Yii;
use yii\rest\ViewAction;
use common\components\helpers\FileInfo;

/**
 * Description of FileAction
 *
 * @author Vladislav
 */
class FileDownloadAction extends ViewAction {
    
    public function run($id) {
        
        $model = parent::run($id);
        
        $filename = $model->getFilenameFull();
        $fileInfo = FileInfo::get($filename);
        Yii::$app->response->xSendFile($filename, $model->title, [
            'mimeType' => $fileInfo['mime_type'],
            'inline' => true,
        ]);
        return;
        
    }
    
}
