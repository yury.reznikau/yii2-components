<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components\controllers;
use Yii;
use yii\helpers\Url;

/**
 * Description of Controller
 *
 * @author Vladislav
 */
class WebController extends \yii\web\Controller {
    
    public function getModule() {
        $modules = $this->getModules();
        return end($modules);
    }
    
    public function getReturnUrlCallback() {
        return function($id) {
            if (!$url = Yii::$app->request->post('returnUrl')) {
                $url = Url::to(['index']);
            }
            return $url;
        };
    }
    
}
