<?php

namespace common\components\controllers;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use Yii;
use yii\helpers\Json;
use common\components\helpers\Application;

abstract class PublicController extends DefaultController
{
    public $isAdmin;
    public $adminIndex = false;

    public function init() {
        parent::init();
        $this->isAdmin = Application::isAdmin();
    }
    
    public function beforeAction($action) {
        
        if (!parent::beforeAction($action)) {
            return false;
        }

        $crud = [
            'create' => 'create', 
            'update' => 'update', 
            'delete' => 'update'
        ];
        
        if (isset($crud[$action->id])) {
            if (!$this->checkAccess($crud[$action->id])) {
                throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
            }    
        }
        
        return true;
    }
    
    public function actions() {
        
        $actions = parent::actions();
        
        $actions['index'] = array_merge($actions['index'], [
            'viewName' => $this->isAdmin && $this->adminIndex ? $this->adminIndex : 'index',
            'searchAttributesCallback' => function(\yii\db\ActiveRecord $searchModel) {
                $attributes = [];
                if (!Application::isAdmin()) {
                    $attributes['user_id'] = $this->user->id;
                }
                return $attributes;
            },
        ]);        
        
        return $actions;
    }
    
    public function loadModel($id, $edit = false, $modelClass = NULL)
    {
        
        if ($model = parent::loadModel($id, $edit, $modelClass)) {
            
            if (!$this->isAdmin && $model->user_id !== $this->user->id) {
                $model = false;
            }
        }
        
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
            
        return $model;
    }

}
